
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.List"%>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>CloverLeaf</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- STYLESHEETS -->
	<!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link href='https://fonts.googleapis.com/css?family=Cabin' rel='stylesheet'>

	<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- DATE RANGE PICKER -->

	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
	<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>

<body class="body_bg1">
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				    aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">
					<img src="logo_inner.png" alt="CloverLeaf" />
				</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">

				<ul class="nav navbar-nav navbar-right">
					<li class="active">
						<a href="login.html">Logout</a>
					</li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>
	<div class="container cont_top">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 abt_border"" id="text">
				<p id="text">With <span class="big_font_inPara">shelfPoint</span>, we give brands and retailers a new way to attract, engage and convert shoppers as they walk past products in store. Audited pilot results with a major <span class="big_font_inPara">US food retailer</span> showed double digit sales uplift in a mainstream product category.</p>

						<p id="text">And it does not stop there. We harness shopper behavior data at the shelf edge, and turn it into actionable insights, enabling marketers to make smarter decisions and optimize campaigns.</p>
			</div>
		</div>
	</div>
	<section id="login" class="visible">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-md-offset-4">
					<div class="login-box-plain">
					<form>
  <input type="hidden" name="username" id="username" value="${username}" /></form>
							<form action="new_account.jsp" method="POST">
							<button type="submit" class="btn btn_blu">Add New Company</button></form>
							
						<form role="form" action="reports.jsp" method="POST" id="dashboardForm">
						
								<div class="divide-20"></div>
								
							<div class="divide-20"></div>
							<div class="form-group">
								<label for="exampleInputEmail1">Account Name</label>
								 <select name="accountName" class="form-control" id="accountName" >
									<!-- <option>Atlanta Beverages</option><option>Jean Coutu</option> -->
									<c:forEach items="${list}" var="item">
									    <option>
									    ${item}
									    </option>
									  </c:forEach>
								 </select>
								
							</div>
							
							<div class="form-group">
								<label for="exampleInputPassword1">Process</label>
								 <select  name="processName" class="form-control" id="processName">
                  						<option>Azure Table to Blob</option>
                  						<option>ETL</option>
						                <option>SQL Server to Spark</option>
						                <option>PowerBI Report</option>


                 				 </select>
								
							</div>

							<button type="submit" id="submit" class="btn btn_grn" onclick="showDiv()">Start</button>

						</form>
						<!-- SOCIAL LOGIN -->
						 

					 
					</div>
					
            <div id="notificationDiv" >
            <h3 class="three">Notification</h3>
              <p  id="statusDiv" ></p>
            
          </div>
				</div>
				
			</div>
			
		</div>
	</section>
<script>
  
    $(document).ready(function(){
	    $("#notificationDiv").hide();
	    //alert("start")
	 /*   var username = $("#username").val();
	    if(username == "admin"){
	    	$("#accountName").html("<option>Atlanta Beverages</option><option>Jean Coutu</option>");
	    }else if(username == "atlanta"){
	    	$("#accountName").html("<option>Atlanta Beverages</option>");
	    }else if(username == "jean"){
	    	$("#accountName").html("<option>Jean Coutu</option>");
	    }*/
	});  
	$( "#dashboardForm" ).submit(function( event ) {
		$("#notificationDiv").hide();
	   //alert( $("#processName").val() + " process started" );
	  var processName = $("#processName").val();
	  var accountName =$("#accountName").val();
	//  action="./login.htm" method="POST"
	$("#notificationDiv").show();
	$("#statusDiv").html("<p>" + processName +" for " + $("#accountName").val() + " is in Progress</p>" );
	 event.preventDefault();
	 $.ajax({
		  type: "POST",
		  url: './dashboard.htm',
		  data: {
				accountName:$("#accountName").val(),
				processName:$("#processName").val()
			},
		  success: function(data){
			  //alert("data: "+data);
			if(processName == "PowerBI Report"){
			    var reports = data.split(" ");
				$("#statusDiv").html("<p>" + processName +" for " + accountName + " is Completed</p>" );
				window.open(reports[0]);
				window.open(reports[1]);
			}else{
				// alert(processName + ' process completed');
				$("#statusDiv").html("<p>" + processName +" for " + accountName + " is Completed</p>" );
			}
		  },
		  error: function(textStatus, errorThrown){
			$("#statusDiv").html("<p>" + processName +" for " + accountName + " is Failed</p>" );

		  }
		});
	});
	 function showDiv(){
  		var x = document.getElementById("statusDiv");
  		//x.style.background-color = "red";	
  	} 

</script>
<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>

</html>