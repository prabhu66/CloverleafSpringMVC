<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>CloverLeaf</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- STYLESHEETS -->
	<!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">

	<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- DATE RANGE PICKER -->

	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
</head>

<body class="body_bg1">
<script >
$( "#addAccount" ).submit(function( event ) {
$.ajax({
	  type: "POST",
	  url: './newaccount.htm',
	  data: {
		  newCompany:$("#newCompany").val(),
		  connectionString:$("#connectionString").val(),
		  etlPath:$("#etlPath").val()
		},
		success: function(data) {
			
			window.location.href = 'welcome.jsp';
		}
});
});
</script>
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				    aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">
					<img src="logo_inner.png" alt="CloverLeaf" />
				</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">

				<ul class="nav navbar-nav navbar-right">
					<li class="active">
						<a href="login.html">Logout</a>
					</li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>
	
	<section id="login" class="visible">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-md-offset-4">
					<div class="login-box-plain">

						<form role="form" action="./newaccount.htm" method="POST" id="addAccount">
								<div class="divide-20"></div>
							
							<div class="divide-20"></div>
							<div class="form-group">
								<label for="exampleInputEmail1">New Company</label>
								 
								<input type="text" class="form-control" id="newCompany" name="newCompany">
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Connection String</label>
								 
								<input type="text" class="form-control" id="connectionString" name="connectionString">
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">ETL Path</label>
								 
								<input type="text" class="form-control" id="etlPath" name="etlPath">
							</div>

							<button type="submit" class="btn btn_grn">Submit</button>

						</form>
						<!-- SOCIAL LOGIN -->
						 

					 
					</div>
				</div>
			</div>
		</div>
	</section>


</body>

</html>