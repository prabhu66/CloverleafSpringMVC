package com.cloverleaf;

import java.util.Date;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class ShopperTrackingData extends TableServiceEntity {
	private String partitionKey;
	private String rowKey;
	private Date timestamp;
	private UUID eventId;
	private UUID sensorRegistryId;
	private String value;
	private String created;
	private String modified;
	public String getPartitionKey() {
		return partitionKey;
	}
	public void setPartitionKey(String partitionKey) {
		this.partitionKey = partitionKey;
	}
	public String getRowKey() {
		return rowKey;
	}
	public void setRowKey(String rowKey) {
		this.rowKey = rowKey;
	}
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	public UUID getEventId() {
		return eventId;
	}
	public void setEventId(UUID eventId) {
		this.eventId = eventId;
	}
	public UUID getSensorRegistryId() {
		return sensorRegistryId;
	}
	public void setSensorRegistryId(UUID sensorRegistryId) {
		this.sensorRegistryId = sensorRegistryId;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getModified() {
		return modified;
	}
	public void setModified(String modified) {
		this.modified = modified;
	}
	
	
}

