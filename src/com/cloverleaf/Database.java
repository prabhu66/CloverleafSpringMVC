package com.cloverleaf;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
public class Database {
	
	private static Connection conn;
	static
	{
		try {
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		 conn= DriverManager.getConnection("jdbc:sqlserver://devcloverleaf.database.windows.net:1433;database=Cloverleaf_AppData;user=cloverleaf@devcloverleaf;password=Cl0verle@f!;encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30");
		}catch(Exception e) {}
	}
	static public int saveToAccountInfo(String newCompany,String connectionString,String etlPath)
	{
		try {
			PreparedStatement pst=conn.prepareStatement("insert into account_info  values (?,?,?)");
			pst.setString(1,newCompany);
			pst.setString(2, connectionString);
			pst.setString(3,etlPath);
			return pst.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
	}
	static public List<String> companyNames()
	{
		try {
			PreparedStatement pst=conn.prepareStatement("select new_company from account_info");
			
			ResultSet result= pst.executeQuery();
			ArrayList<String>list=new ArrayList<>();
			while(result.next())
			{
				list.add(result.getString(1));
			}
			return list;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
